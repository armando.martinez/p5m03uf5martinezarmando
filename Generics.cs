﻿using System;
using P4M03Uf4ArmandoMartinez;


namespace P5M03UF5MartinezArmando
{
    public class TaulaOrdenable <T> : IOrdenable
    {
        public T[] DatosElemento;
        int i { get; set; }

        public TaulaOrdenable()
        {
            this.DatosElemento = new T[] { };
            this.i = 0;
        }
        public TaulaOrdenable(int z)
        {
            this.DatosElemento = new T[z];
            this.i = 0;
        }
        public int Comparar(IOrdenable x)
        {
            TaulaOrdenable<T> y = (TaulaOrdenable<T>)x;
            if (y.i == this.i)
            {
                return 0;
            }
            else if (y.i > this.i)
            {
                return -1;
            }
            return 1;
        }

        public int Capacitat()
        {
            return DatosElemento.Length;
        }

        public int NrElements()
        {
            int n = 0;
            foreach(T element in DatosElemento) { if (element != null) n++; }
            return n;
        }

        public int Afegir(T newElement)
        {
            if (this.NrElements() == this.Capacitat())
            {
                return -1;
            }
            else if(newElement == null)
            {
                return -2;
            }
            DatosElemento[NrElements()] = newElement;
            return 0;
        }

        public T ExemplarAt(int n)
        {
            if (n < 0 && n > this.Capacitat()) { return default(T); }
            return DatosElemento[n];
        }

        public T ExtreureAt(int n)
        {
            if (n < 0 && n > this.Capacitat()) { return default(T); }
            T copy = DatosElemento[n];
            DatosElemento[n] = default(T);
            return copy;
        }
        public void Buidar()
        {
            for (int j = 0; j < this.Capacitat(); j++)
            {
                this.DatosElemento[j] = default(T);
            }
        }

        public void Visualitzar()
        {
            Console.WriteLine($"Capacitat: {this.Capacitat()}");
            Console.WriteLine($"Elements: {this.NrElements()}\n");
            foreach (T val in this.DatosElemento)
            {
                if(val != null) Console.WriteLine(val.ToString());
            }
        }
    }

    class TaulaOrdenableFiguraGeometrica : TaulaOrdenable<FiguraGeometrica>
    {
        public TaulaOrdenableFiguraGeometrica(int z)
        {
            if (z < 0) z = 10;
            this.DatosElemento = new FiguraGeometrica[z];
        }
    }
}