﻿using System;
using P5M03UF5MartinezArmando;
using P4M03Uf4ArmandoMartinez;

namespace P5M03UF5MartinezArmando
{
    class Program
    {
        static void Main()
        {
            Title("TAULA ORDENABLE FIGURA GEOMETRICA");
            TaulaOrdenable<FiguraGeometrica> taula = new TaulaOrdenableFiguraGeometrica(50);
            taula.Afegir(new Rectangle(2, "Primer", ConsoleColor.Red, 2, 4.5));
            taula.Afegir(new Cercle(4, "Segon", ConsoleColor.Red, 4.5));
            taula.Afegir(new Cercle(5, "Tercer", ConsoleColor.Blue, 6));
            taula.Afegir(new Triangle(8, "Quart", ConsoleColor.Green, 2, 4.5));
            taula.Afegir(new Triangle(10, "Cinquè", ConsoleColor.Blue, 5, 3));
            taula.Visualitzar();

            Console.ReadLine();
            Title("PROVES");

            Title("ExemplarAt(3)");
            Console.WriteLine(taula.ExemplarAt(3).ToString());
            Console.ReadLine();

            Title("ExtreureAt(2)");
            Console.WriteLine(taula.ExtreureAt(2));
            Title("             ");
            taula.Visualitzar();
            Console.ReadLine();

            Title("Buidar()");
            taula.Buidar();
            taula.Visualitzar();
        }

        static void Title(string a)
        {
            Console.WriteLine(a);
            foreach(char b in a) { Console.Write("="); }
            Console.WriteLine();
        }
    }
}
