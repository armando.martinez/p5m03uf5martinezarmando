﻿using System;

namespace P4M03Uf4ArmandoMartinez
{
    public abstract class FiguraGeometrica : IOrdenable
    {
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }
        public abstract double Area();
        public override abstract int GetHashCode();

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
        public int Comparar(IOrdenable y)
        {
            FiguraGeometrica x = (FiguraGeometrica)y;
            if (x.codi == this.codi)
            {
                return 0;
            }
            else if (x.codi > this.codi)
            {
                return 1;
            }
            return -1;
        }

    }

    public class Rectangle : FiguraGeometrica
    {
        public double width { get; set; }
        public double height { get; set; }

        public Rectangle()
        {
            this.codi = 1;
            this.nom = "Rectangle";
            this.color = ConsoleColor.White;
            this.width = 1;
            this.height = 1;
        }
        public Rectangle(int codi, string nom, ConsoleColor color, double width, double height)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.width = width;
            this.height = height;
        }
        public Rectangle(Rectangle prevRectangle)
        {
            this.codi = prevRectangle.codi;
            this.nom = prevRectangle.nom;
            this.color = prevRectangle.color;
            this.width = prevRectangle.width;
            this.height = prevRectangle.height;
        }

        override public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nBase: {this.width}\nAltura: {this.height}\n";
        }
        override public int GetHashCode()
        {
            int n = 0;
            foreach (char c in this.nom) { n += Convert.ToInt32(c); }
            return ((this.codi + n) << (Convert.ToInt32(this.height % 10))) + Convert.ToInt32(this.color) + Convert.ToInt32(this.width);
        }
        public double Perimetre()
        {
            return (this.height * 2) + (this.width * 2);
        }
        override public double Area()
        {
            return this.height * this.width;
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }

            // Dibuja el rectangulo
            Console.ForegroundColor = this.color;
            for (int i = 0; i < this.height; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < this.width; j++)
                {
                    Console.Write("* ");
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }
    }

    public class Triangle : FiguraGeometrica
    {
        public double width { get; set; }
        public double height { get; set; }

        public Triangle()
        {
            this.codi = 1;
            this.nom = "Triangle";
            this.color = ConsoleColor.White;
            this.width = 1;
            this.height = 1;
        }
        public Triangle(int codi, string nom, ConsoleColor color, double width, double height)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.width = width;
            this.height = height;
        }
        public Triangle(Triangle prevTriangle)
        {
            this.codi = prevTriangle.codi;
            this.nom = prevTriangle.nom;
            this.color = prevTriangle.color;
            this.width = prevTriangle.width;
            this.height = prevTriangle.height;
        }

        override public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nBase: {this.width}\nAltura: {this.height}\n";
        }
        override public int GetHashCode()
        {
            int n = 0;
            foreach (char c in this.nom) { n += Convert.ToInt32(c); }
            return ((this.codi + n) << (Convert.ToInt32(this.width % 10))) + Convert.ToInt32(this.color) + Convert.ToInt32(this.height);
        }
        public double Perimetre()
        {
            return this.height + this.width + Math.Sqrt(Math.Pow(this.height, 2) + Math.Pow(this.width, 2));
        }
        override public double Area()
        {
            return this.height * this.width / 2;
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }
            Console.WriteLine();

            // Dibuja el triangulo (se puede arreglar idk)
            Console.ForegroundColor = this.color;

            for (int i = 1; i <= this.height; i++)
            {
                for (int j = 1; j <= this.width; j++)
                {
                    if (i + j >= (Convert.ToDouble(this.height + this.width) / 2.0) + 0.5)
                    {
                        Console.Write("* ");
                    }
                }
                Console.WriteLine();
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }

    public class Cercle : FiguraGeometrica
    {
        public double radi { get; set; }

        public Cercle()
        {
            this.codi = 1;
            this.nom = "Cercle";
            this.color = ConsoleColor.White;
            this.radi = 1;
        }
        public Cercle(int codi, string nom, ConsoleColor color, double radi)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.radi = radi;
        }
        public Cercle(Cercle prevCercle)
        {
            this.codi = prevCercle.codi;
            this.nom = prevCercle.nom;
            this.color = prevCercle.color;
            this.radi = prevCercle.radi;
        }

        override public string ToString()
        {
            return $"Codi: {this.codi}\nNom: {this.nom}\nColor: {this.color.ToString()}\nRadi: {this.radi}\n";
        }

        override public int GetHashCode()
        {
            int n = 0;
            foreach (char c in this.nom) { n += Convert.ToInt32(c); }
            return ((this.codi + n) << (Convert.ToInt32(this.radi % 10))) + Convert.ToInt32(this.color);
        }
        public double Perimetre()
        {
            return 2 * Math.PI * this.radi;
        }
        override public double Area()
        {
            return Math.PI * Math.Pow(this.radi, 2);
        }
        public void Draw()
        {
            // Escribe el nombre
            Console.WriteLine(this.nom);
            for (int i = 0; i < this.nom.Length; i++) { Console.Write('-'); }
            Console.WriteLine();

            // Dibuja el circulo
            Console.ForegroundColor = this.color;
            for (double y = -this.radi; y <= this.radi; y++)
            {
                for (double x = -this.radi; x <= this.radi; x++)
                {
                    if (x * x + y * y <= this.radi * this.radi)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }


    class ProvaFigures
    {
        static void Try(string[] args)
        {
            Rectangle test1 = new Rectangle(34, "Test", ConsoleColor.Blue, 4, 6);
            Triangle test2 = new Triangle(34, "Test", ConsoleColor.Blue, 4, 6);
            Cercle test3 = new Cercle(4, "Copia", ConsoleColor.Green, 5);
            Cercle copyOfTest3 = new Cercle(test3);

            test1.Draw();
            Console.WriteLine(test1.GetHashCode() + "\n");

            test2.Draw();
            Console.WriteLine(test2.GetHashCode() + "\n");
            
            test3.Draw();
            Console.WriteLine(test3.GetHashCode() + "\n");
            
            copyOfTest3.Draw();
            Console.WriteLine(copyOfTest3.GetHashCode() + "\n");

            Console.WriteLine(test1.Equals(test2));

            Console.WriteLine(test3.Equals(copyOfTest3));

        }
    }
}
