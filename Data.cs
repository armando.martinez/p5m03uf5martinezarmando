﻿using System;

namespace P4M03Uf4ArmandoMartinez
{
    class Data : IOrdenable
    {
        public int dia { get; set; }
        public int mes { get; set; }
        public int any { get; set; }
        private int[] diesEnMes { get; set; }

        public Data()
        {
            this.diesEnMes = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            if ((any % 4 == 0 && any % 100 != 0) || any % 400 == 0) { this.diesEnMes[1] = 29; }
            this.dia = 1;
            this.mes = 1;
            this.any = 1980;
        }
        public Data(int dia, int mes, int any)
        {
            this.diesEnMes = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            if ((any % 4 == 0 && any % 100 != 0) || any % 400 == 0) { this.diesEnMes[1] = 29; }
            if (any > 0 && mes > 0 && mes < 13 && dia > 0 && dia <= this.diesEnMes[mes - 1])
            {
                this.dia = dia;
                this.mes = mes;
                this.any = any;
            } 
            else
            {
                this.dia = 1;
                this.mes = 1;
                this.any = 1980;
            }
        }
        public Data(Data data)
        {
            this.dia = data.dia;
            this.mes = data.mes;
            this.any = data.any;
        }

        private int ComptaDias()
        {
            int dias = 0;
            dias += this.any * 365 + (this.any / 4);
            for (int i = 1; i < this.mes; i++)
            { dias += this.diesEnMes[i]; }
            dias += this.dia;
            return dias;
        }
        public void SumaDias(int n)
        {
            int dias = ComptaDias();
            dias += n;
            this.any = Convert.ToInt32(Convert.ToDouble(dias) / 365.25);
            dias = Convert.ToInt32(Convert.ToDouble(dias) % 365.25);
            int mes = 0;
            while (dias >= this.diesEnMes[mes])
            {
                dias -= this.diesEnMes[mes];
                mes++;
            }
            this.mes = mes + 1;
            this.dia = dias;
        }
        public bool CompareDate(Data data)
        {
            return this.ComptaDias() == data.ComptaDias();
        }
        public int DaysBetween(Data data)
        {
            return this.ComptaDias() - data.ComptaDias();
        }

        public int Comparar(IOrdenable y)
        {
            Data x = (Data)y;
            if(this.ComptaDias() == x.ComptaDias())
            {
                return 0;
            }
            else if (this.ComptaDias() > x.ComptaDias())
            {
                return 1;
            }
            return -1;
        }
    }
    class Temporal
    {
        static void ProvaData()
        {
            Console.WriteLine("Hello World!");
            Data date = new Data();
            Data date2 = new Data(5, 3, 2023);
            Data date3 = new Data(27, 2, 2023);

            date.SumaDias(392);
            date2.SumaDias(-15);

            Console.WriteLine($"{date.dia}, {date.mes}, {date.any}");
            Console.WriteLine($"{date2.dia}, {date2.mes}, {date2.any}");

            Console.WriteLine(date2.CompareDate(date3));
            Console.WriteLine(date.DaysBetween(date2));
        }
    }
}
